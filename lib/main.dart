import 'package:flutter/material.dart';

import './screens/home_screen.dart';
import './screens/user_detail_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'WebServices',
      theme: ThemeData(
        primarySwatch: Colors.purple,
      ),
      routes: {
        '/': (ctx) => HomeScreen(),
        'user_detail_screen': (ctx) => UserDetailScreen(),
      }, 
    );
  }
}