import 'package:dio/dio.dart';

class HttpService {
  final Dio _dio;

  HttpService()
  : _dio = Dio(BaseOptions(baseUrl: 'https://reqres.in')) 
  {
    //initilizeInterceptors();
  }

  void initilizeInterceptors() {
    _dio.interceptors.add(InterceptorsWrapper(
      onError: (error, errorInterceptorHandler) {
        print(error.message);
      },
      onRequest: (request, requestInterceptorHandler) {
        print("${request.method} ${request.path}");
      },
      onResponse: (response, responseInterceptorHandler) {
        print(response.statusCode);
      },
    ));
  }

  Future<Response> getRequest(String endpoint) async {
    return await _dio.get(endpoint);
  }
}
