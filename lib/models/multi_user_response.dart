import 'package:json_annotation/json_annotation.dart';

import './user.dart';

part 'multi_user_response.g.dart';

@JsonSerializable()
class MultiUserResponse {
  
  @JsonKey(name: "data")
  List<User>? users;

  MultiUserResponse();

  factory MultiUserResponse.fromJson(Map<String, dynamic> json) =>
      _$MultiUserResponseFromJson(json);
  Map<String, dynamic> toJson() => _$MultiUserResponseToJson(this);
}
