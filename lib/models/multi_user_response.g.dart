// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'multi_user_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MultiUserResponse _$MultiUserResponseFromJson(Map<String, dynamic> json) {
  return MultiUserResponse()
    ..users = (json['data'] as List<dynamic>?)
        ?.map((e) => User.fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$MultiUserResponseToJson(MultiUserResponse instance) =>
    <String, dynamic>{
      'data': instance.users,
    };
