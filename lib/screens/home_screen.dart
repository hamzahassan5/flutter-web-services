import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

import '../models/multi_user_response.dart';
import '../models/user.dart';
import '../services/http_service.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  MultiUserResponse? multiUserResponse;
  List<User>? users;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Users'),
      ),
      body: FutureBuilder(
        future: HttpService().getRequest('/api/users?page=2'),
        builder: (BuildContext ctx, AsyncSnapshot<Response> snapshot) {
          if (snapshot.hasError) {
            return Center(
              child: Text('An Error Occured!'),
            );
          }

          if (snapshot.hasData) {
            if (snapshot.data?.statusCode == 200) {
              multiUserResponse =
                  MultiUserResponse.fromJson(snapshot.data?.data);
              users = multiUserResponse?.users;

              return ListView.builder(
                itemCount: users?.length,
                itemBuilder: (ctx, i) {
                  return Card(
                    child: ListTile(
                      leading: CircleAvatar(
                        backgroundImage:
                            NetworkImage(users![i].avatar.toString()),
                      ),
                      title: Text(
                          "${users![i].firstName.toString()} ${users![i].lastName.toString()}"),
                      onTap: () => Navigator.of(ctx).pushNamed(
                        'user_detail_screen',
                        arguments: users![i].email.toString(),
                      ),
                    ),
                  );
                },
              );
            } else {
              return Center(
                child: Text('Network Request Failed!'),
              );
            }
          }

          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}
